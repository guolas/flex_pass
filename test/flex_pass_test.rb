require 'test_helper'
require 'models/user'
require 'models/user_sha'
require 'models/user_other_column'
require 'models/user_not_valid_coder'
require 'models/oauthed_user'
require 'models/visitor'
require 'models/administrator'

class FlexPassTest < ActiveSupport::TestCase
  setup do
    @user = User.new
    @user_sha = UserSHA.new
    @user_other_column = UserOtherColumn.new
    @visitor = Visitor.new
    @oauthed_user = OauthedUser.new
  end

  test "truth" do
    assert_kind_of Module, FlexPass
  end
  test "blank password" do
    @user.password = @visitor.password = ''
    assert !@user.valid?(:create), 'user should be invalid'
    assert @visitor.valid?(:create), 'visitor should be valid'
  end

  test "nil password" do
    @user.password = @visitor.password = nil
    assert !@user.valid?(:create), 'user should be invalid'
    assert @visitor.valid?(:create), 'visitor should be valid'
  end

  test "blank password doesn't override previous password" do
    @user.password = 'test'
    @user.password = ''
    assert_equal @user.password, 'test'
  end

  test "password must be present" do
    assert !@user.valid?(:create)
    assert_equal 1, @user.errors.size
  end

  test "match confirmation" do
    @user.password = @visitor.password = "thiswillberight"
    @user.password_confirmation = @visitor.password_confirmation = "wrong"

    assert !@user.valid?
    assert @visitor.valid?

    @user.password_confirmation = "thiswillberight"

    assert @user.valid?
  end

  test "authenticate with BCrypt" do
    @user.password = "secret"

    assert_not_equal @user.password, @user.password_digest.to_s, 'Password digest should not be equal to the password'
    assert !@user.authenticate("wrong")
    assert @user.authenticate("secret")
  end

  test "authenticate with SHA2" do
    @user_sha.password = "secret"
    
    assert_not_equal @user_sha.password, @user_sha.password_digest.to_s, 'Password digest should not be equal to the password'
    assert !@user_sha.authenticate("wrong"), '"wrong" should be wrong'
    assert @user_sha.authenticate("secret"), '"secret" should be right'
  end
  
  test "using a non existent digest column should fail" do
    assert_raise RuntimeError do
      require 'models/user_non_existent_column'
    end
  end

  test "authenticate using a different digest column" do
    @user_other_column.password = "secret"

    assert_not_equal @user_other_column.password, @user_other_column.other_digest_column.to_s, 'Password digest should not be equal to the password'
    assert_nil @user_other_column.password_digest
    assert !@user_other_column.authenticate("wrong"), '"wrong" should be wrong'
    assert @user_other_column.authenticate("secret"), '"secret" should be right'
  end

  test "invalid coder should use Bcrypt instead" do
    assert_equal UserNotValidCoder.coder, BCrypt::Password, 'When invalid coder is specified, BCrypt should be used instead'
  end

  test "User should not be created with blank digest" do
    assert_raise RuntimeError do
      @user.run_callbacks :create
    end
    @user.password = "supersecretpassword"
    assert_nothing_raised do
      @user.run_callbacks :create
    end
  end

  test "Oauthed user can be created with blank digest" do
    assert_nothing_raised do
      @oauthed_user.run_callbacks :create
    end
  end

  test "blank password_confirmation does not result in a confirmation error" do
    @user.password = ""
    @user.password_confirmation = ""
    assert @user.valid?(:update), "user should be valid"
  end

  test "will not save if confirmation is blank but password is not" do
    @user.password = "password"
    @user.password_confirmation = ""
    assert_not @user.valid?(:create)

    @user.password_confirmation = "password"
    assert @user.valid?(:create)
  end
end
