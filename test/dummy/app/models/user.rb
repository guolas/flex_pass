require 'flex_pass/flex_secure_password'

class User < ActiveRecord::Base
  has_flexible_secure_password :validations => false, :coder => FlexPass::Coders::SHA512Hash
end
