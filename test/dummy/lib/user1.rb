class User1
  extend ActiveModel::Callbacks
  include ActiveModel::Validations
  include FlexPass::FlexSecurePassword
  
  attr_accessor :password_digest, :password_salt

  define_model_callbacks :create

  has_flexible_secure_password
  
end
