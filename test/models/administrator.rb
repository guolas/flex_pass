class Administrator
  extend ActiveModel::Callbacks
  include ActiveModel::Validations
  include FlexPass::FlexSecurePassword

  define_model_callbacks :create

  attr_accessor :name, :password_digest

  has_flexible_secure_password

end
