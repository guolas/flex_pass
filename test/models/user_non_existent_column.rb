class UserNonExistentColumn
  extend ActiveModel::Callbacks
  include ActiveModel::Validations
  include FlexPass::FlexSecurePassword
  
  define_model_callbacks :create

  attr_accessor :password_digest, :password_salt

  has_flexible_secure_password :digest_column => 'non_existent_column'
  
end
