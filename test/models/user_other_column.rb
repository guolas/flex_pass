class UserOtherColumn
  extend ActiveModel::Callbacks
  include ActiveModel::Validations
  include FlexPass::FlexSecurePassword
  
  define_model_callbacks :create

  attr_accessor :password_digest, :other_digest_column, :password_salt

  has_flexible_secure_password :digest_column => 'other_digest_column'
  
end
