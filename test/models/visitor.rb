class Visitor
  extend ActiveModel::Callbacks
  include ActiveModel::Validations
  include FlexPass::FlexSecurePassword

  define_model_callbacks :create

  attr_accessor :password_digest, :password_confirmation

  has_flexible_secure_password(validations: false)

end
