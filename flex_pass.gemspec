$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "flex_pass/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "flex_pass"
  s.version     = FlexPass::VERSION
  s.license     = 'MIT'
  s.authors     = ["Juan Jose Garcia Fernandez"]
  s.email       = ["guolas@gmail.com"]
  s.homepage    = "https://github.com/guolas/flex_pass"
  s.summary     = "Extension of `has_secure_password`."
  s.description = "Extension of `has_secure_password` that enables the possibility to use a coder different from `BCrypt`, and to choose a digest column different from `password_digest`."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 3.2.13"

  s.add_development_dependency "sqlite3"
end
