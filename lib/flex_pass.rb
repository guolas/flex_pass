module FlexPass
  autoload :FlexSecurePassword, 'flex_pass/flex_secure_password'
  module Coders
    autoload :SHA2Hash, 'flex_pass/coders/sha2_hash'
    autoload :SHA256Hash, 'flex_pass/coders/sha256_hash'
    autoload :SHA512Hash, 'flex_pass/coders/sha512_hash'
  end
  ActiveRecord::Base.send :include, FlexPass::FlexSecurePassword
end
