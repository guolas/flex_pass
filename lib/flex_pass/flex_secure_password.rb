module FlexPass
  module FlexSecurePassword
    extend ActiveSupport::Concern

    module ClassMethods
      
      attr_accessor :coder
      attr_accessor :digest_column

      def has_flexible_secure_password(options = {})

        attr_reader :password
        
        include InstanceMethodsOnActivation

        if options.fetch(:validations, true)
          validates_confirmation_of :password, if: lambda { |m| m.password.present? }
          validates_presence_of     :password, on: :create
          validates_presence_of     :password_confirmation, if: lambda { |m| m.password.present? }

          before_create { raise "Password digest missing on new record" if password_digest.blank? }
        end
        
        if (options.has_key? :coder) && (options[:coder].respond_to? 'create')
          self.coder = options[:coder]
        else
          begin
            gem 'bcrypt-ruby', '~> 3.0.0'
            require 'bcrypt'
          rescue LoadError
            $stderr.puts "You don't have bcrypt-ruby installed in your application. Please add it to your Gemfile and run bundle install"
            raise
          end
          self.coder = BCrypt::Password
        end
        
        self.digest_column = options.fetch(:digest_column, 'password_digest')
        
        unless self.instance_methods.include? self.digest_column.to_s.to_sym
          unless (self.ancestors.include? ActiveRecord::Base) && (self.attribute_names.include? self.digest_column.to_s)
            raise RuntimeError, "\'#{self.digest_column}\' column is not defined, please select an existing column to store the password digest"
          end
        end

        if respond_to?(:attributes_protected_by_default)
          def self.attributes_protected_by_default #:nodoc:
            super + [self.digest_column]
          end
        end
      end
    end

    module InstanceMethodsOnActivation
      def authenticate(unencrypted_password)
        coder = self.class.coder
        digest_column = self.class.digest_column
        coder.new(self.public_send(digest_column)) == unencrypted_password && self
      end

      def password=(unencrypted_password)
        unless unencrypted_password.blank?
          @password = unencrypted_password
          coder = self.class.coder
          digest_column = self.class.digest_column
          self.public_send("#{digest_column}=", coder.create(unencrypted_password))
        end
      end

      def password_confirmation=(unencrypted_password)
        @password_confirmation = unencrypted_password
      end
    end
  end
end

