module FlexPass
  module Coders
    class SHA256Hash < SHA2Hash

      class << self
        
        def create(secret)
          super(secret, 256)
        end    
      end
  
      def initialize(raw_hash)
        super(raw_hash, 256)
      end
  
      def ==(secret)
        super(secret)
      end

  
    end
  end
end
