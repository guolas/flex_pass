module FlexPass
  module Coders
    class SHA512Hash < SHA2Hash

      class << self
        
        def create(secret)
          super(secret, 512)
        end    
      end
  
      def initialize(raw_hash)
        super(raw_hash, 512)
      end
  
      def ==(secret)
        super(secret)
      end

  
    end
  end
end
