module FlexPass
  module Coders
    class SHA2Hash < String

      class << self
        
        def create(secret, bit_len = 256)
          SHA2Hash.new(Digest::SHA2.new(bit_len).hexdigest(secret), bit_len)
        end    
      end
  
      def initialize(raw_hash, bit_len = 256)
        self.replace(raw_hash)
        @bit_len = bit_len
      end
  
      def ==(secret)
        super(Digest::SHA2.new(@bit_len).hexdigest(secret))
      end
  
    end
  end
end
