# FlexPass

Inspired by Aaron Patterson's [talk](http://www.youtube.com/watch?v=kWOAHIpmLAI).

At around min 26:40 he makes a comment about `has_secure_password` and how rigid it is, and how nice it would be to be able to use other methods for encrypting the password, and to make use of the serialization functionality of Rails.

It does not does all of that, but it now allows to use other hashing mechanisms, not just BCrypt.

This is essentially my first code in Ruby/Rails, so it probably is not as perfect as it should be, but it seems to work.

### Usage

First load the gem:

    require 'flex_pass'

Then the usage is pretty similar to that of `has_secure_password`, and it consists on calling the method:

    has_flexible_secure_password

which is a rather long name ...

Called just like that withing the model will yield almost exactly the same behavior as `has_secure_password`, as it can be seen from the test suite, that is literally mimicked from the one used for `ActiveModel::SecurePassword`.

What this method accepts now is an extra parameter `coder` that is the name of a class implementing an interface similar to that of `BCrypt::Password`. This is:

+ A `new` method that allows to initialize the instance with a hash value for authentication purposes.
+ A `create` method that takes an unencrypted password as input and returns an instance with the corresponding hash value.
+ A `==` that compares the current hash value with an unencrypted password value.

This can be seen, for example, in the following class, found in `flex_pass/coders/` folder, that will perform the hashing using SHA2 algorithm:

    class SHA2Crypt < String

      class << self
        def create(secret)
          SHA2Crypt.new(Digest::SHA2.new(256).hexdigest(secret))
        end
      end
      def initialize(raw_hash)
        self.replace(raw_hash)
      end
        
      def ==(secret)
        super(Digest::SHA2.new(256).hexdigest(secret))
        end
    end

In order to use this, assuming that the model is able to find the class, something like the following would be done:

	class User < ActiveRecord::Base
	  has_flexible_secure_password :coder => SHA2Crypt
	end

It also accepts the option to choose the name of the column where the password digest is going to be stored, so that now it does not necessarily have to be `password_digest`. It tries to asess if the column exists or not, raising a `RuntimeError` in case it does not exist. The ussage is:

    class User < ActiveRecord::Base
      has_flexible_secure_password :digest_colum => 'other_colum_to_store_digest'
    end

It will store the password digest in the column `other_column_to_store_digest`.

Both options, `:coder` and `:digest_column`, can be arbitrarily combined.

